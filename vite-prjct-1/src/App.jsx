import React, { useState } from 'react';
import './App.css';
import myImage from './components/ModalBody/img/image.png';
import Modal from './components/Modal/Modal';
import ModalWrapper from './components/ModalWrapper/ModalWrapper';
import ModalHeader from './components/ModalHeader/ModalHeader';
import ModalBody from './components/ModalBody/ModalBody';
import ModalFooter from './components/ModalFooter/ModalFooter';
import ModalClose from './components/ModalClose/ModalClose';
import Button from './components/Button/Button';



function App() {
  const [firstModalOpen, setFirstModalOpen] = useState(false);
  const [secondModalOpen, setSecondModalOpen] = useState(false);

  return (
    <>
      <Button onClick={() => setFirstModalOpen(true)}>Open first modal</Button>
      <Button onClick={() => setSecondModalOpen(true)}>Open second modal</Button>

      {firstModalOpen && (
        <ModalWrapper>
          <Modal size='large'>
            <ModalHeader>
              <ModalClose onClick={() => setFirstModalOpen(false)} />
            </ModalHeader>
            <ModalBody>
            <img src={myImage} alt="#" className='body-img' />
            <div>
              <h1>Product Delete!</h1>
              <p className='body-text'>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</p>
            </div>
            </ModalBody>
            <ModalFooter
              firstText="NO, CANCEL"
              secondaryText="YES, DELETE" />
          </Modal>
        </ModalWrapper>
      )}



      {secondModalOpen && (
        <ModalWrapper>
          <Modal size='small'>
            <ModalHeader>
              <ModalClose onClick={() => setSecondModalOpen(false)} />
            </ModalHeader>
            <ModalBody>
              <div>
                <h1 className='body-h'>Add Product “NAME”</h1>
                <p>Description for you product</p>
              </div>
            </ModalBody>
            <ModalFooter
              firstText="ADD TO FAVORITE"
            />
          </Modal>
        </ModalWrapper>
      )}

    </>
  );
}

export default App;



