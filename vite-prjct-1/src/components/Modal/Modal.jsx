import React from 'react';
import './Modal.css'


function Modal({ size, children }) {
  return (
    <div className={`modal-back ${size}`}>
      <div className='modal'>
        {children}
      </div>
    </div>
  )
};

export default Modal;