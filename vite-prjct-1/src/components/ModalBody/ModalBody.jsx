import React from 'react';
import './ModalBody.css'

function ModalBody({ children }) {
  return (
    <div>
      {children}
    </div>
  )
};

export default ModalBody;