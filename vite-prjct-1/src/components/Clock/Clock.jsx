import { useEffect, useState } from 'react';

 function Clock() {

    const [time, setTime] = useState(new Date().toLocaleTimeString());

    useEffect(() => {
    const clock = setInterval(() => {
      setTime(new Date().toLocaleTimeString())
    }, 1000);

    return () => clearInterval(clock);
    },[]);


    return (
        <div>clock here: {time}</div>
    )
 }

 export default Clock;