import React from 'react';
import Button from '../Button/Button';
import './ModalFooter.css'


function ModalFooter({ firstText, secondaryText, firstClick, secondaryClick }) {
  return (
    <div className="modal-footer">
      {firstText && <Button classNames="btn-first" onClick={firstClick}>{firstText}</Button>}
      {secondaryText && <Button classNames="btn-second" onClick={secondaryClick}>{secondaryText}</Button>}
    </div>
  );
};

export default ModalFooter;