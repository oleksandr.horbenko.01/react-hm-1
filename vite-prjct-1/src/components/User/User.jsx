import React from "react";
import { useEffect, useState } from "react";


function User() {

    const [name, setName] = useState("Aleksandr")

    useEffect(() => {
        console.log('name changed');
    }, [name]);
    

    return (
        <div>
            <div>User name: {name}</div>
            <button onClick={()=> setName('Jack')}>change name</button>
        </div>
    )
}

export default User;