import React from 'react';
import './ModalClose.css'

function ModalClose({ onClick }) {
  return (
    <button className="modal-close" onClick={onClick}>
      X
    </button>
  );
};

export default ModalClose;